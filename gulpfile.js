'use strict';
const $ 			= require('gulp-load-plugins')();
const gulp 			= require('gulp');
const del 			= require('del');
const mail 			= require('gulp-mail');
const argv 			= require('yargs').argv;

const smtpInfo = {
	auth: {
		user: 'devemailcreate@gmail.com',
		pass: 'devemailcreatepass'
	},
	host: 'smtp.gmail.com',
	secureConnection: true,
	port: 465
}

const optionsJade 	= {
	src: 'dev/*.jade',
	dist: 'build'
}

const optionsServe 	= {
	src: ['build']
}

const bs 			= require('browser-sync').create();

const optionsImg 	= {
	path: 'dev/img/*.*',
	dist: 'build/img/',
	crop: false
}


gulp.task('clean', function(){
	return del(['build/*']);
})


gulp.task('jade', function(){
	return gulp.src( optionsJade.src )
		.pipe( $.plumber() )

		.pipe( $.changed( 'build', { extension: '.html' } ) )

		.pipe ( $.jade({
			pretty: true
		}) )

		.pipe( gulp.dest( optionsJade.dist ) );

});

gulp.task('img', function(){
	return gulp.src( optionsImg.path )
		.pipe( $.plumber() )
		.pipe( $.image({
			zopflipng: false
		}) )
		.pipe( gulp.dest( optionsImg.dist ) );
});

gulp.task( 'serve', function(){
	bs.init({
		server: optionsServe.src
	});

	bs.watch('dev/**/*.jade').on('change', gulp.series('jade', bs.reload) )
});


gulp.task('copy:img', function() {
	return gulp.src('dev/img/*.*')
		.pipe( gulp.dest( optionsImg.dist ) )
});

gulp.task('watch:img', function() {
	gulp.watch('dev/img/*.*', gulp.series('img'))
});





gulp.task('send', function() {
	//return gulp.src(argv.mail)
	return gulp.src('build/*.html')
		.pipe(mail({
			subject: 'Test email',
			to: [
				'memonanofsa@gmail.com',
				'alex@huskyjam.com'
			],
			from: 'Def <dev@gmail.com>',
			smtp: smtpInfo
		}))
});




gulp.task('default', gulp.series(
		'clean',
		'jade',
		'img',
		gulp.parallel( 'watch:img', 'serve' )
	)
);